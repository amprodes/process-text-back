# process-text-back

Node.js + MySQL API for process-text-back, Authentication and Registration

### Built With

This section show Frameworks/libraries used to bootstrap the project.

* [Node.js](https://nodejs.org/)
* [Express.js](https://expressjs.com/)
* [sequalize.js](https://sequelize.org/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

This is how you set up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is a list of software you need to use the app.
* npm
* nodejs
* mysql

### Installation

_Below is an example of how you can install and setting up your app._
 
1. Clone the repo
   ```sh
   git clone git@gitlab.com:amprodes/process-text-back.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Adjust info for your mysql credentials `config.js`
   ```js
    {
        "database": {
            "host": "localhost",
            "port": 3306,
            "user": "root",
            "password": "",
            "database": "processtextdb"
        },
        "secret": "PWFHW9F9YF98SYF98SYDF98YSDFP"
    }
   ```
4. Now you can run the project with the following command
   ```sh
   npm run start
   ```
<p align="right">(<a href="#top">back to top</a>)</p>