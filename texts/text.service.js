﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');

module.exports = { 
    getAll,
    getById,
    getTextByUser,
    getTextByUserAndTextId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Text.findAll();
}

async function getById(id) {
    return await getText(id);
}

async function create(params) {  
    // save text
    await db.Text.create(params);
}

async function update(id, params) {
    const text = await getText(id);

    // validate
    const titleChanged = params.title && text.title !== params.title;
    if (titleChanged && await db.Text.findOne({ where: { title: params.title } })) {
        throw 'Text "' + params.title + '" is already taken';
    }
 
    // copy params to user and save
    Object.assign(text, params);
    await text.save();

    return text.get();
}

async function _delete(id) {
    const text = await getText(id);
    await text.destroy();
}

// helper functions

async function getText(id) {
    const text = await db.Text.findByPk(id);
    if (!text) throw 'Text not found';
    return text;
}

async function getTextByUser(userId) {
    const text = db.Text.findAll({ where: { userId } })

    if (!text) throw 'Text not found';
    return text;
}

async function getTextByUserAndTextId(userId, textId) {
    const text = db.Text.findOne({ where: { userId, id:textId } })

    if (!text) throw 'Text not found';
    return text;
}
