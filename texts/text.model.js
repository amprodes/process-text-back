const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        userId: { type: DataTypes.INTEGER, allowNull: false },
        title: { type: DataTypes.STRING, allowNull: false },
        text: { type: DataTypes.STRING, allowNull: false },
        unique: { type: DataTypes.INTEGER, allowNull: false },
        wordslength: { type: DataTypes.INTEGER, allowNull: false },
        words: { type: DataTypes.STRING, allowNull: false }
    };
  
    return sequelize.define('Text', attributes);
}