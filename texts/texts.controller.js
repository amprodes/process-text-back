﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request'); 
const textService = require('./text.service');

// route
router.post('/save', saveSchema, save);
router.get('/user/:userId', getTextByUser);
router.get('/:textId/user/:userId', getTextByUserAndTextId);
router.get('/:id',  getById);
router.put('/:id', updateSchema, update);
router.delete('/:id',  _delete);

module.exports = router;
 
function saveSchema(req, res, next) {
    const schema = Joi.object({
        userId: Joi.number().required(),
        title: Joi.string().required(),
        text: Joi.string().required(),
        unique: Joi.number().required(),
        wordslength: Joi.number().required(),
        words: Joi.string().required()
    });
    validateRequest(req, next, schema);
}

function save(req, res, next) {
    textService.create(req.body)
        .then(() => res.json({ message: 'Save successful' }))
        .catch(() => res.json({ message: 'Save Error' }));
}

function getAll(req, res, next) {
    textService.getAll()
        .then(texts => res.json(texts))
        .catch(next);
}

function getCurrent(req, res, next) {
    res.json(req.text);
}

function getTextByUserAndTextId(req, res, next) {
    textService.getTextByUserAndTextId(req.params.userId, req.params.textId)
        .then(text => res.json(text))
        .catch(next);
}

function getTextByUser(req, res, next) {
    textService.getTextByUser(req.params.userId)
        .then(text => res.json(text))
        .catch(next);
}

function getById(req, res, next) {
    textService.getById(req.params.id)
        .then(text => res.json(text))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        title: Joi.string().empty(''),
        text: Joi.string().empty(''),
        unique: Joi.number().empty(''),
        wordslength: Joi.number().empty(''),
        words: Joi.string().empty('')
    });
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    textService.update(req.params.id, req.body)
        .then(text => res.json(text))
        .catch(next);
}

function _delete(req, res, next) {
    textService.delete(req.params.id)
        .then(() => res.json({ message: 'User deleted successfully' }))
        .catch(next);
}